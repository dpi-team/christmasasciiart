# Christmas ASCII Art

An application which creates and shows the Christmas Art such as chrismas trees and stars using ASCII symbols

## About app

Using this app, you can easily generate 2 shapes (tree and star) in 3 types of sizes:

● S - small: 5 lines height

● M - medium: 7 lines height

● L - large: 11 lines height

If user doesn't pick a size or a shape, app will pick random size and shape.

### Requirements

PHP 7.1.0

Composer

### Installing

```
git clone https://bitbucket.org/dpi-team/christmasasciiart
composer install
```

## Running

From CLI
```
php src/App/index.php run tree l
```
From browser
```
http://localhost/src/App/index.php?mode=run&shape=tree&size=l
```

### Running Tests
From CLI
```
php src/App/index.php test
```
From browser
```
http://localhost/src/App/index.php?mode=test
```


## Authors

* [Ilkin Alibayli](https://www.linkedin.com/in/ilkin-alibayli)

## License

This project is licensed under the MIT License