<?php

namespace App\Size;

use App\Contract\SizeContract;


/**
 * Class SizeAbstract
 * @package App\Size
 */
abstract class SizeAbstract implements SizeContract {
	/**
	 * @var
	 */
	protected $height;

	/**
	 * @return int
	 */
	public function getHeight (): int {
		return $this->height;
	}
}