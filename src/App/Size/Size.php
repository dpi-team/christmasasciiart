<?php

namespace App\Size;


/**
 * Class Size
 * @package App\Size
 */
class Size extends SizeAbstract {

	/**
	 * Size constructor.
	 *
	 * @param int $height
	 */
	public function __construct ( int $height ) {
		$this->height = $height;
	}
}