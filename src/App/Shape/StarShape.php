<?php

namespace App\Shape;


/**
 * Class StarShape
 * @package App\Shape
 */
class StarShape extends ShapeAbstract {


	/**
	 *
	 * Generation of array of lines (without additional symbols)
	 *
	 * @return array
	 */
	public function createPictureArray (): array {
		//Dividing star to the 2 pyramids and excluding central line to draw it separately
		$size = ( $this->size->getHeight () / 2 ) - 1;
		$pictureArray = [];
		$symbolsCount = 0;

		//On the star design we see that the step of star grow coefficient is +4 and step of free space grow coefficient is +2
		for ( $line = 0, $freeSpacesCount = 0; $line < $size; $line++ ) {

			if ( $line == 0 ) {
				//Setting the first line as only 1 symbol
				$symbolsCount = 1;
			}

			$pictureArray[ $line ] = $this->addFreeSpaces ( $freeSpacesCount ) . $this->drawSymbols ( $symbolsCount );

			$freeSpacesCount += 2; //step
			$symbolsCount += 4; //step
		}

		//Connection original and reversed array to get the down side of star
		$pictureArray = $this->connectArrays ( $pictureArray );
		$pictureArray = $this->addWrapSymbols ( $pictureArray );

		return $pictureArray;
	}

	/**
	 * @param array $array
	 *
	 * @return array
	 */
	public function connectArrays ( array $array ) {
		$copyArray = array_reverse ( $array );
		array_shift ( $copyArray );

		$array = array_merge ( $array, $copyArray );

		return $array;
	}

	/**
	 *
	 * Adding wrap symbols to all the 4 sides of star
	 *
	 * @param array $pictureArray
	 *
	 * @return array
	 */
	public function addWrapSymbols ( array $pictureArray ): array {
		$wrapper = str_replace ( $this->symbol, $this->wrapSymbol, $pictureArray[ 0 ] );

		//Adding wrap symbol to the top and to the bottom side of star
		array_unshift ( $pictureArray, $wrapper );
		array_push ( $pictureArray, $wrapper );


		//Finding the central vertical line to add wrapper symbols to the beginning and the end of it
		$centralLine = (int)floor ( count ( $pictureArray ) / 2 );
		foreach ( $pictureArray as $key => $value ) {
			if ( $key === $centralLine ) {
				$pictureArray[ $key ] = $this->wrapSymbol . $value . $this->wrapSymbol;
			} else {
				$pictureArray[ $key ] = $this->symbolSeparator . $value . $this->symbolSeparator;
			}
		}

		return $pictureArray;
	}
}