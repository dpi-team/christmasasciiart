<?php

namespace App\Shape;

/**
 * Class TreeShape
 * @package App\Shape
 */
class TreeShape extends ShapeAbstract {

	/**
	 *
	 * Generation of array of lines
	 *
	 * @return array
	 */
	public function createPictureArray (): array {
		$size = $this->size->getHeight () - 1;
		$pictureArray = [];
		$symbolsCount = 0;

		//On the tree design we see that the step of star grow coefficient is +2 and step of free space grow coefficient is +1
		for ( $line = 0, $freeSpacesCount = -1; $line < $size; $line++ ) {

			if ( $line == 0 ) {
				//Setting the first line as only 1 symbol
				$symbolsCount = 1;
			}

			$pictureArray[ $line ] = $this->addFreeSpaces ( $freeSpacesCount ) . $this->drawSymbols ( $symbolsCount );

			$symbolsCount += 2;
			$freeSpacesCount += 1;
		}


		$pictureArray = $this->addWrapSymbols ( $pictureArray );

		return $pictureArray;
	}

	/**
	 *
	 * Adding wrap symbol to the top of the tree
	 *
	 * @param array $pictureArray
	 *
	 * @return array
	 */
	public function addWrapSymbols ( array $pictureArray ): array {
		$wrapper = str_replace ( $this->symbol, $this->wrapSymbol, $pictureArray[ 0 ] );

		array_unshift ( $pictureArray, $wrapper );

		return $pictureArray;
	}
}