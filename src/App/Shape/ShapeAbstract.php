<?php

namespace App\Shape;

use App\Contract\SizeContract;
use App\Contract\ShapeContract;

/**
 * Class Shape
 * @package App\Shape
 */
abstract class ShapeAbstract implements ShapeContract {

	/**
	 * @var SizeContract
	 */
	protected $size;

	/**
	 * @var string
	 */
	protected $lineSeparator;

	/**
	 * @var string
	 */
	protected $symbolSeparator;

	/**
	 * @var string
	 */
	protected $wrapSymbol;

	/**
	 * @var string
	 */
	protected $symbol;

	/**
	 * @var array
	 */
	private $pictureArray;


	/**
	 * Shape constructor.
	 *
	 * @param SizeContract $size
	 * @param string $lineSeparator
	 * @param string $symbolSeparator
	 * @param string $wrapSymbol
	 * @param string $symbol
	 */
	public function __construct ( SizeContract $size, string $lineSeparator, string $symbolSeparator, string $wrapSymbol, string $symbol ) {
		$this->size = $size;
		$this->lineSeparator = $lineSeparator;
		$this->symbolSeparator = $symbolSeparator;
		$this->wrapSymbol = $wrapSymbol;
		$this->symbol = $symbol;
		$this->pictureArray = $this->createPictureArray ();
	}

	/**
	 *
	 * Drawing symbols
	 *
	 * @param int $count
	 *
	 * @return string
	 */
	public function drawSymbols ( int $count ): string {
		//Getting repeating symbol $count times
		return str_pad ( '', $count, $this->symbol );
	}

	/**
	 *
	 * Drawing free space (empty char(s))
	 *
	 * @param int $freeSpacesCount
	 *
	 * @return string
	 */
	public function addFreeSpaces ( int $freeSpacesCount ): string {

		$count = $this->size->getHeight () - 3;

		if ( $freeSpacesCount == 0 ) {
			return str_pad ( '', $count, ' ' );
		}

		$freeSpacesCount = $count - $freeSpacesCount;

		//Getting repeating free space symbol $count times
		return str_pad ( '', $freeSpacesCount, ' ' );
	}

	/**
	 *
	 * Drawing function, it converts generated array to string
	 *
	 * @return string
	 */
	public function draw (): string {
		$picture = '';

		foreach ( $this->pictureArray as $value ) {
			$picture .= $value . $this->lineSeparator;
		}

		return $picture;
	}
}