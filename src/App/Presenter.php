<?php

namespace App;

/**
 * Class Presenter
 * @package App
 */
/**
 * Class Presenter
 * @package App
 */
class Presenter {

	/**
	 * @var array
	 */
	private $configData;
	/**
	 * @var string
	 */
	private $size;
	/**
	 * @var string
	 */
	private $shape;


	/**
	 * Presenter constructor.
	 *
	 * @param array $configData
	 * @param null|string $size
	 * @param null|string $shape
	 */
	public function __construct ( array $configData, ?string $size, ?string $shape ) {
		$this->configData = $configData;

		$this->size = $size ?? $this->getRandomSize ();
		$this->shape = $shape ?? $this->getRandomShape ();

		if ( isset( $size ) && !$this->checkSizeExists ( $size ) ) {
			$this->size = $this->getRandomSize ();
		}

		if ( isset( $shape ) && !$this->checkShapeExists ( $shape ) ) {
			$this->shape = $this->getRandomShape ();
		}
	}

	/**
	 * @return string
	 */
	public function getRandomShape (): string {
		return $this->configData[ 'shape' ][ array_rand ( $this->configData[ 'shape' ] ) ];
	}

	/**
	 * @return string
	 */
	public function getRandomSize (): string {
		return array_rand ( $this->configData[ 'size' ] );
	}


	/**
	 * @param string $size
	 *
	 * @return bool
	 */
	public function checkSizeExists ( string $size ): bool {
		return key_exists ( $size, $this->configData[ 'size' ] );
	}

	/**
	 * @param string $shape
	 *
	 * @return bool
	 */
	public function checkShapeExists ( string $shape ): bool {
		return in_array ( $shape, $this->configData[ 'shape' ] );
	}

	/**
	 * @return string
	 */
	public function getSize (): string {
		return $this->configData[ 'size' ][ $this->size ];
	}

	/**
	 * @return string
	 */
	public function getShape (): string {
		return $this->shape;
	}
}