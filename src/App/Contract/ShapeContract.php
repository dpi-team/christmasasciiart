<?php

namespace App\Contract;

/**
 * Interface ShapeContract
 * @package App\Contract
 */
Interface ShapeContract {

	/**
	 *
	 * @return string
	 */
	function draw (): string;

	/**
	 * @return array
	 */
	function createPictureArray (): array;

	/**
	 * @param array $pictureArray
	 *
	 * @return array
	 */
	function addWrapSymbols ( array $pictureArray ): array;

}