<?php

namespace App\Contract;

/**
 * Interface SizeContract
 * @package App\Contract
 */
Interface SizeContract {

	/**
	 * @return int
	 */
	function getHeight (): int;

}