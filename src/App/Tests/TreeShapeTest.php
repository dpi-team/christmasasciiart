<?php

namespace App\Tests;

/**
 * Class TreeShapeTest
 * @package App\Tests
 */
class TreeShapeTest extends AbstractShapeTester {

	/** @test */
	public function it_shows_valid_s_size_tree_test () {
		$fakeTree = file_get_contents ( $this->shapesPath . 'treeS.txt' );
		$shellResult = shell_exec ( 'php ' . $this->scriptPath . ' run tree s' );

		$this->assertEquals ( $fakeTree, $shellResult );
	}

	/** @test */
	public function it_shows_valid_m_size_tree_test () {
		$fakeTree = file_get_contents ( $this->shapesPath . 'treeM.txt' );
		$shellResult = shell_exec ( 'php ' . $this->scriptPath . ' run tree m' );

		$this->assertEquals ( $fakeTree, $shellResult );
	}

	/** @test */
	public function it_shows_valid_l_size_tree_test () {
		$fakeTree = file_get_contents ( $this->shapesPath . 'treeL.txt' );
		$shellResult = shell_exec ( 'php ' . $this->scriptPath . ' run tree l' );

		$this->assertEquals ( $fakeTree, $shellResult );
	}
}