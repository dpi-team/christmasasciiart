<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;

/**
 * Class AbstractShapeTester
 * @package App\Tests
 */
abstract class AbstractShapeTester extends TestCase {
	/**
	 * @var string
	 */
	protected $shapesPath;
	/**
	 * @var string
	 */
	protected $scriptPath;

	/**
	 * AbstractShapeTester constructor.
	 *
	 * @param null|string $name
	 * @param array $data
	 * @param string $dataName
	 */
	public function __construct ( ?string $name = NULL, array $data = [], string $dataName = '' ) {
		parent::__construct ( $name, $data, $dataName );
		$this->shapesPath = __DIR__ . DIRECTORY_SEPARATOR . 'shapes' . DIRECTORY_SEPARATOR;
		$this->scriptPath = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'index.php';
	}
}