<?php

namespace App\Tests;

use DirectoryIterator;

/**
 * Class AnyShapeTest
 * @package App\Tests
 */
class AnyShapeTest extends AbstractShapeTester {
	
	/** @test */
	public function it_shows_a_shape_on_empty_input_params () {
		$shapes = [];

		foreach ( new DirectoryIterator( $this->shapesPath ) as $shapeFile ) {
			if ( $shapeFile->isDot () ) {
				continue;
			}

			$file = $this->shapesPath . $shapeFile->getFilename ();

			$shapes[ $shapeFile->getFilename () ] = file_get_contents ( $file );
		}

		$shellResult = shell_exec ( 'php ' . $this->scriptPath );

		$this->assertContains ( $shellResult, $shapes );
	}
}