<?php

namespace App\Tests;

/**
 * Class StarShapeTest
 * @package App\Tests
 */
class StarShapeTest extends AbstractShapeTester {

	/** @test */
	public function it_shows_valid_s_size_star_test () {
		$fakeTree = file_get_contents ( $this->shapesPath . 'starS.txt' );
		$shellResult = shell_exec ( 'php ' . $this->scriptPath . ' run star s' );

		$this->assertEquals ( $fakeTree, $shellResult );
	}

	/** @test */
	public function it_shows_valid_m_size_star_test () {
		$fakeTree = file_get_contents ( $this->shapesPath . 'starM.txt' );
		$shellResult = shell_exec ( 'php ' . $this->scriptPath . ' run star m' );

		$this->assertEquals ( $fakeTree, $shellResult );
	}

	/** @test */
	public function it_shows_valid_l_size_star_test () {
		$fakeTree = file_get_contents ( $this->shapesPath . 'starL.txt' );
		$shellResult = shell_exec ( 'php ' . $this->scriptPath . ' run star l' );

		$this->assertEquals ( $fakeTree, $shellResult );
	}
}