<?php

header ( 'Content-Type: text/plain' );

$config = require __DIR__ . '/Config/app.php';

if ( $config[ 'debug' ] ) {
	ini_set ( 'display_errors', 1 );
	ini_set ( 'display_startup_errors', 1 );
	error_reporting ( E_ALL );
}

function runTests () {
	echo shell_exec ( './phpunit src/App/Tests' );
	exit;
}

//Checking if it run using CLI or a Browser
if ( php_sapi_name () === 'cli' ) {
	require __DIR__ . '/../../vendor/autoload.php';

	//RUN using command: index.php run tree l
	if ( isset( $argv[ 1 ] ) && $argv[ 1 ] === 'test' ) {
		runTests ();
	}

	$shapeParam = $argv[ 2 ] ?? NULL;
	$sizeParam = $argv[ 3 ] ?? NULL;
} else {
	require $_SERVER[ 'DOCUMENT_ROOT' ] . '/vendor/autoload.php';

	$shapeParam = $_GET[ 'shape' ] ?? NULL;
	$sizeParam = $_GET[ 'size' ] ?? NULL;

	if ( isset( $_GET[ 'mode' ] ) && $_GET[ 'mode' ] === 'test' ) {
		runTests ();
	}
}


use App\Presenter;
use App\Size\Size;
use App\Shape\StarShape;
use App\Shape\TreeShape;

$presenter = new Presenter( $config, $sizeParam ?? NULL, $shapeParam ?? NULL );
$sizeParam = $presenter->getSize ();
$shapeParam = $presenter->getShape ();

$sizeObject = new Size( $sizeParam );

switch ( $shapeParam ) {
	case 'tree':
		$shape = new TreeShape( $sizeObject, PHP_EOL, chr ( 32 ), '+', 'X' );
		break;
	case 'star':
		$shape = new StarShape( $sizeObject, PHP_EOL, chr ( 32 ), '+', 'X' );
		break;
}

echo $shape->draw ();